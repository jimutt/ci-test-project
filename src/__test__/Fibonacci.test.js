import { Fibonacci } from '../Fibonacci'

test('Performs correct fibonacci calculations', () => {
  const fib = new Fibonacci()

  expect(fib.fib(1)).toBe(1)
  expect(fib.fib(2)).toBe(1)
  expect(fib.fib(3)).toBe(2)
  expect(fib.fib(10)).toBe(55)
})
