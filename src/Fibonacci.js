export class Fibonacci {
  constructor() {
    this._curNo = 0
  }

  next() {
    this._curNo++

    return this.fib(this._curNo)
  }

  fib(no) {
    if (no <= 0) return 0
    if (no > 0 && no < 3) return 1

    let res = 0
    let prevOldRes = 1
    let prevRes = 1

    for (let i = 2; i < no; i++) {
      res = prevOldRes + prevRes
      prevOldRes = prevRes
      prevRes = res
    }

    return res
  }
}
