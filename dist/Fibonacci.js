"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Fibonacci = exports.Fibonacci = function () {
  function Fibonacci() {
    _classCallCheck(this, Fibonacci);

    this._curNo = 0;
  }

  _createClass(Fibonacci, [{
    key: "next",
    value: function next() {
      this._curNo++;

      return this._fib(this._curNo);
    }
  }, {
    key: "_fib",
    value: function _fib(no) {
      if (no <= 0) return 0;
      if (no > 0 && no < 3) return 1;

      var res = 0;
      var prevOldRes = 1;
      var prevRes = 1;

      for (var i = 2; i < no; i++) {
        res = prevOldRes + prevRes;
        prevOldRes = prevRes;
        prevRes = res;
      }

      return res;
    }
  }]);

  return Fibonacci;
}();